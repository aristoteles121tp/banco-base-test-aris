# BUILDER
FROM node:18-alpine as builder

WORKDIR /app

COPY . .

RUN apk --no-cache add \
  bash \
  g++ \
  ca-certificates \
  lz4-dev \
  musl-dev \
  cyrus-sasl-dev \
  openssl-dev \
  make \
  python3

RUN apk add --no-cache --virtual .build-deps gcc zlib-dev libc-dev bsd-compat-headers py-setuptools bash

RUN npm i -g typescript && npm i
RUN npm run build:unix


# PRODUCTION
FROM node:18-alpine

ARG PORT

WORKDIR /app

COPY --from=builder /app/dist .

COPY package*.json .

RUN apk --no-cache add \
  bash \
  g++ \
  ca-certificates \
  lz4-dev \
  musl-dev \
  cyrus-sasl-dev \
  openssl-dev \
  make \
  python3

RUN apk add --no-cache --virtual .build-deps gcc zlib-dev libc-dev bsd-compat-headers py-setuptools bash

RUN npm i

CMD [ "node", "index.js" ]

EXPOSE 8080
