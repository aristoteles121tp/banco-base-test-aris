## Prueba técnica de banco base

- Este proyecto contiene una API REST con funciones CRUD para edición de notas, una conexión a kafka en la nube (confluent) y un contenedor con una base de datos de Mongodb y el contenedor que contiene el proyecto de node

## Requisitos previos 
- [docker](https://www.docker.com/get-started/)
- [docker-compose](https://docs.docker.com/compose/install/)


## Creación de imagen del proyecto mediante Dockerfile
- Ejecutar el siguiente comando en la raíz del proyecto:
```bash
$ docker build --tag banco_base_test
```
Nota: esto puede tardar unos minutos en terminar de crear la imagen.

## Crear contenedores de mongo y el del proyecto de node (banco_base_test)
- Ejecutar el siguiente comando en la raíz del proyecto:
```bash
$ docker-compose up -d
```

## Configuración

En la raíz del proyecto se debe se encuentra el archivo `docker-compose.yml`, en ese archivo ya se encuentran las variables de entorno seteadas para cada contenedor, para efectos de prueba, se esta utilizando kafka mediante un servicio en la nube llamado confluent, esta configuración esta en el archivo mencionado de las lineas 24 a la 27, el proyecto funciona solamente levantando los contenedores con docker compose pero si se desea configurar otro servicio de kafka, se deben editar las siguientes variables dentro del archivo:
```bash
KAFKA_SERVER: <server_kafka>:<server_port_kafka>
KAFKA_USERNAME: <user_name>
KAFKA_PWD: <password>
KAFKA_TOPICNAME: <topic_name>
```



## Instalación de módulos

Ejecutar el siguiente comando en la raíz del proyecto:
```bash
$ npm i
```

## Pruebas unitarias
Para ejecutar las pruebas unitarias, se debe instalar las siguientes dependencias previamente

- [node v18.18.0](https://nodejs.org/es/download)
- [typescript](https://www.typescriptlang.org/download)
- [Python 2.7.17](https://www.python.org/downloads/release/python-2717/)

Y despues, ejecutar el siguiente comando:

```bash
$ npm run test
```

## Postman
En la raiz del proyecto, en la ruta `src\docs\postman` se agregaron dos archivos, uno de los request y otro del enviroment para poder consumir los servicios del proyecto
- Para efectos de pruebas con kafka, en el servicio `get` de `home`, manda un mensaje al kafka, este evento ya se recibe en el código y se guarda en la base de datos de mongo de manera automatica

## Areas de oportunidad
- Por el tiempo que tuve para realizar la prueba no alcance a agregar una clase Logger para el correcto debuging y trazabilidad de una request, ni el Middleware para el manejo de errores
- Tampoco me dio tiempo de agregar validaciones para los datos de entrada de los servicios con `class-validatos` o `joi`