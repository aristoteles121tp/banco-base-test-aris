/* eslint-disable @typescript-eslint/no-explicit-any */
import 'reflect-metadata';
import express from 'express';
import { v4 } from 'uuid';
import cors from 'cors';
import { InversifyExpressServer } from 'inversify-express-utils';
import { AppContainer } from './packages/api/modules';
import { Application } from 'express';
import morgan from 'morgan';
import moment from 'moment';

class App {

  public app: express.Application;

  private assignIDToRequest(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) {
    (req as any).id = v4();
    return next();
  }

  private configMorgan(app: express.Application) {
    app.use(morgan(function (tokens, req, res) {
      return [
        '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',
        '\n' + moment().format('YYYY-MM-DD HH:mm:ss'),
        `[ ${(req as any).id} ]`,
        tokens.method(req, res),
        tokens.url(req, res),
        `\nbody: ${JSON.stringify((req as any).body || {}, null, 4)}`,
        `\nquery: ${JSON.stringify((req as any).query || {}, null, 4)}`,
      ].join(' ')
    }, { immediate: true }));
    app.use(morgan(function (tokens, req, res) {
      return [
        moment().format('YYYY-MM-DD HH:mm:ss'),
        `[ ${(req as any).id} ]`,
        tokens.method(req, res),
        tokens.url(req, res),
        tokens.status(req, res),
        tokens.res(req, res, 'content-length'),
        '-',
        tokens['response-time'](req, res),
        'ms',
        '\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',
      ].join(' ')
    }));
    return;
  }

  private config(app: express.Application) {
    app.use(express.urlencoded({ extended: false }));
    app.use(express.json());
    app.use(cors());
    app.use(this.assignIDToRequest);
    this.configMorgan(app);
  }

  constructor() {
    const iversifyServer = new InversifyExpressServer(AppContainer());
    iversifyServer
      .setConfig(async (app: Application) => {
        this.config(app);
      })
      .setErrorConfig((app) => {
        app.use((err: any, req: any, res: any) => {
          console.error(err.stack);
          res.status(500).send('Something broke!');
        });

      });

    this.app = iversifyServer.build();
  }

}

export const app = new App().app;