/* eslint-disable @typescript-eslint/no-explicit-any */
import 'dotenv/config';
import { app } from "./app";
import { KafkaService } from "./packages/thirdparties";
import { MongooseService } from './packages/db/mongodb/mongoose.connection';
import { ModelEventKafka } from './packages/db/mongodb';


const PORT: number = process.env.PORT ? +process.env.PORT : 3000;

async function startKafka() {
  const kafkaService: KafkaService = new KafkaService();
  await kafkaService.startServices();
  (global as any).kafkaService = kafkaService;
}

async function startMongodb() {
  const mongooseService: MongooseService = new MongooseService();
  mongooseService.setAutoReconnect();
  await mongooseService.initConnection();
  await ModelEventKafka.createCollection();
}

app.listen(PORT, async () => {
  console.log(`Server runing in port: ${PORT}`);
  console.log('Starting Kafka');
  await Promise.all([
    startKafka(),
    startMongodb()
  ]);
});