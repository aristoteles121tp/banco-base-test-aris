import { inject } from "inversify";
import { HOME_SYMBOLS, IHomeUC } from "../../../core";
import { httpGet, BaseHttpController,  } from "inversify-express-utils";
import { Request } from 'express';

class HomeController extends BaseHttpController {

  public constructor(
    @inject(HOME_SYMBOLS.UC_HOME) private homeUC: IHomeUC
  ) {
    super();
  }

  @httpGet('')
  greating(req: Request) {
    const reqId: string = req.id;
    console.log(`Executing HomeController.greating from request id: ${reqId}`);
    kafkaService.sendMessage('Consume service home', 'HomeController.greating');
    return this.homeUC.greating();
  }
}

export {
  HomeController
};