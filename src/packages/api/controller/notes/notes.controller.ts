import { INotesUC, NOTES_SYMBOLS } from "../../../core";
import { httpGet, BaseHttpController, httpPost, httpPut, httpDelete,  } from "inversify-express-utils";
import { Request } from 'express';
import { inject } from "inversify";

class NotesController extends BaseHttpController {

  public constructor(
    @inject(NOTES_SYMBOLS.UC_NOTES) private notesUC: INotesUC
  ) {
    super();
  }

  @httpGet('')
  getAllNotes(req: Request) {
    const reqId: string = req.id;
    console.log(`Executing NotesController.getAllNotes from request id: ${reqId}`);
    return this.notesUC.getAllNotes();
  }

  @httpPost('')
  createOneNote(req: Request) {
    const reqId: string = req.id;
    console.log(`Executing NotesController.createOneNote from request id: ${reqId}`);
    return this.notesUC.createOneNote(req.body);
  }

  @httpPut('/:id')
  updateOneNote(req: Request) {
    const reqId: string = req.id;
    console.log(`Executing NotesController.updateOneNote from request id: ${reqId}`);
    return this.notesUC.updeteNote(req.params.id, req.body);
  }

  @httpDelete('/:id')
  deleteOneNote(req: Request) {
    const reqId: string = req.id;
    console.log(`Executing NotesController.deleteOneNote from request id: ${reqId}`);
    return this.notesUC.deleteNoteById(req.params.id);
  }

  @httpGet('/:id')
  getOneNote(req: Request) {
    const reqId: string = req.id;
    console.log(`Executing NotesController.getOneNote from request id: ${reqId}`);
    return this.notesUC.getNoteById(req.params.id);
  }
  
}

export {
  NotesController
};