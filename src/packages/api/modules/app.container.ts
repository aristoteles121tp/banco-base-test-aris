import { Container } from "inversify";
import { HomeModule } from "./home";
import { NotesModule } from "./notes";

export const AppContainer = (): Container => {
  const container = new Container();
  container.load(HomeModule);
  container.load(NotesModule);
  return container;
}