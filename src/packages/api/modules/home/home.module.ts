import { ContainerModule, interfaces } from "inversify";
import { HomeController } from "../../controller/home";
import { HOME_SYMBOLS, HomeUC, IHomeUC } from "../../../core";
import { controller } from "inversify-express-utils";

export const HomeModule = new ContainerModule((bind: interfaces.Bind) => {
  bind<IHomeUC>(HOME_SYMBOLS.UC_HOME).to(HomeUC);
  controller('/home')(HomeController);
});