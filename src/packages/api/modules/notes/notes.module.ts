import { ContainerModule, interfaces } from "inversify";
import { controller } from "inversify-express-utils";
import { NotesController } from "../../controller";
import { INotesUC, NOTES_SYMBOLS, NotesUC } from "../../../core";
import { DbLocalContainer, NoteCurd } from "../../../db";


export const NotesModule = new ContainerModule((bind: interfaces.Bind) => {
  const notesCrud = DbLocalContainer().get<NoteCurd>(NoteCurd);

  bind<INotesUC>(NOTES_SYMBOLS.UC_NOTES)
    .toConstantValue(
      new NotesUC(notesCrud)
    );

  controller('/notes')(NotesController);
});