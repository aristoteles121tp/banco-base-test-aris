import "reflect-metadata";
import { HomeModule } from '../../../api/modules/home/home.module';
import { Container } from 'inversify';
import { IHomeUC } from './home.uc.interface';
import { HOME_SYMBOLS } from '../constants';


describe("Home use case", () => {
  let homeUC: IHomeUC;
  beforeAll(() => {
    const container = new Container();
    container.load(HomeModule);
    homeUC = container.get<IHomeUC>(HOME_SYMBOLS.UC_HOME);
  });
  test("Home test", () => {
    const greatingString: string = homeUC.greating();
    expect(greatingString).toBeTruthy();
  });
});