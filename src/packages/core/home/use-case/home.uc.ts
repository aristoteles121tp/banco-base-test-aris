import { injectable } from "inversify";
import { IHomeUC } from "./home.uc.interface";

@injectable()
class HomeUC implements IHomeUC {
	greating(): string {
    const greatingStr: string = 'Hello world!';
		console.log('Executing HomeUC.greating');
    return greatingStr;
	}
}

export {
	HomeUC
};