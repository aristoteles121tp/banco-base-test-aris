import { NoteEntity } from "../../../db";

type inputCreateNote = Omit<NoteEntity, 'id'>;
type inputUpdateNote = Partial<Omit<NoteEntity, 'id'>>;

interface IReponseDeleteNotes {
  notesIdsNotDeleted: string[];
}

interface IReponseUpdateNotes {
  notesUpdated: NoteEntity[];
  notesIdsNotUpdated: string[];
}

interface INoteCrud {
  createNotes(data: inputCreateNote[]): NoteEntity[];
  getNoteById(id: string): NoteEntity | null;
  getAllNotes(): NoteEntity[];
  updateNotes(ids: string[], data: inputUpdateNote[]): IReponseUpdateNotes;
  deleteNotes(ids: string[]): IReponseDeleteNotes;
}

export {
  INoteCrud,
  inputCreateNote,
  inputUpdateNote,
  IReponseUpdateNotes,
  IReponseDeleteNotes
};