import { NoteEntity } from "../../../db";
import { inputCreateNote, inputUpdateNote } from "../crud";

interface INotesUC {
  getAllNotes(): NoteEntity[];
  getNoteById(id: string): NoteEntity;
  createOneNote(data: inputCreateNote): NoteEntity;
  updeteNote(id: string, data: inputUpdateNote): NoteEntity;
  deleteNoteById(id: string): boolean;
}

export {
  INotesUC
};