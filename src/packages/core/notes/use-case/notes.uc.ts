import "reflect-metadata";

import { injectable } from "inversify";
import { NoteEntity } from "../../../db";
import { INoteCrud, IReponseDeleteNotes, IReponseUpdateNotes, inputCreateNote, inputUpdateNote } from "../crud";
import { INotesUC } from "./notes.uc.interface";

@injectable()
class NotesUC implements INotesUC {

  public constructor(
    private notesCrud: INoteCrud
  ) { }

  getNoteById(id: string): NoteEntity {
    return this.notesCrud.getNoteById(id) as NoteEntity;
  }

  createOneNote(data: inputCreateNote): NoteEntity {
    const noteCreated: NoteEntity = this.notesCrud.createNotes([data])[0];
    return noteCreated;
  }

  updeteNote(id: string, data: inputUpdateNote): NoteEntity {
    const notesUpdateResponse : IReponseUpdateNotes = this.notesCrud.updateNotes([id], [data]);
    console.log(JSON.stringify({ notesUpdateResponse }, null, 2));
    const [notesUpdated] = notesUpdateResponse.notesUpdated;
    return notesUpdated;
  }

  deleteNoteById(id: string): boolean {
    const notesDeletedResponse: IReponseDeleteNotes = this.notesCrud.deleteNotes([id]);
    console.log(JSON.stringify({ notesDeletedResponse }, null, 2));
    return notesDeletedResponse.notesIdsNotDeleted.length === 0;
  }

  getAllNotes(): NoteEntity[] {
    return this.notesCrud.getAllNotes();
  }

}

export {
  NotesUC
};