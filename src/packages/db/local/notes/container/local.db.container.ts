import { Container } from "inversify";
import { NoteCurd } from "..";


export const DbLocalContainer = (): Container => {
  const container = new Container();
  container.bind<NoteCurd>(NoteCurd).toSelf().inSingletonScope();
  return container;
}