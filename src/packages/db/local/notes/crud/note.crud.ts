import { injectable } from "inversify";
import { INoteCrud, IReponseDeleteNotes, IReponseUpdateNotes, inputCreateNote, inputUpdateNote } from "../../../../core";
import { NoteEntity } from "../entities";
import { v4 } from 'uuid';

@injectable()
class NoteCurd implements INoteCrud {
  
  private notes: NoteEntity[] = [];

  createNotes(data: inputCreateNote[]): NoteEntity[] {
    const inputNotes: NoteEntity[] = [];
    data.forEach(input => {
      inputNotes.push({
        id: v4(),
        ...input
      })
    });
    this.notes.push(...inputNotes);
    return inputNotes;
  }

  getNoteById(id: string): NoteEntity | null {
    return this.notes.find(note => note.id === id) || null;
  }

  getAllNotes(): NoteEntity[] {
      return [...this.notes];
  }

  updateNotes(ids: string[], data: inputUpdateNote[]): IReponseUpdateNotes {
    const notesUpdated: NoteEntity[] = [];
    const notesIdsNotUpdated: string[] = [];

    ids.forEach((id: string, index: number) => {
      const noteIdFound: number = this.notes.findIndex(note => note.id === id);
      if (noteIdFound >= 0) {
        if (data[index].title) {
          this.notes[noteIdFound].title = data[index].title as string;
        }
        if (data[index].content) {
          this.notes[noteIdFound].content = data[index].content as string;
        }
        notesUpdated.push(this.notes[noteIdFound]);
      } else {
        notesIdsNotUpdated.push(id);
      }
    });
  
    return {
      notesUpdated,
      notesIdsNotUpdated
    };
  }

  deleteNotes(ids: string[]): IReponseDeleteNotes {
    const notesIdsNotDeleted: string[] = [];
    ids.forEach((id: string) => {
      const noteIdFound: number = this.notes.findIndex(note => note.id === id);
      if (noteIdFound >= 0) {
        this.notes.splice(noteIdFound, 1);
      } else {
        notesIdsNotDeleted.push(id);
      }
    });

    return {
      notesIdsNotDeleted
    };
  }

}

export {
  NoteCurd
};