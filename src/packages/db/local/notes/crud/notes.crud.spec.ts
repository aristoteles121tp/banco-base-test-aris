import "reflect-metadata";
import { INoteCrud, IReponseDeleteNotes, IReponseUpdateNotes, inputCreateNote, inputUpdateNote } from "../../../../core";
import { DbLocalContainer } from "../container";
import { NoteCurd } from "./note.crud";
import { NoteEntity } from "../entities";
import { faker } from '@faker-js/faker';

let notesCrud: INoteCrud;
const clearAllNotes = () => {
  if (notesCrud) {
    const currenNotes: NoteEntity[] = notesCrud.getAllNotes();
    if (currenNotes?.length) {
      notesCrud.deleteNotes(currenNotes.map(note => note.id));
    }
  }
}

const createNotes = (notesToCreate: number): NoteEntity[] | void => {
  if (notesCrud) {
    const notesInput: inputCreateNote[] = [];
    for(let i = 0; i < notesToCreate; i++) {
      notesInput.push({
        title: faker.word.adjective(),
        content: faker.word.words(),
      });
    }
    return notesCrud.createNotes(notesInput);
  }
};

describe("Notes CRUD - Data base local test", () => {
  
  beforeAll(() => {
    notesCrud = DbLocalContainer().get<NoteCurd>(NoteCurd);
  });
  beforeEach(() => clearAllNotes());

  describe("Create notes test", () => {
    beforeEach(() => clearAllNotes());
    test("Create one note test", () => {
      const noteInput: inputCreateNote = {
        title: faker.word.adjective(),
        content: faker.word.words(),
      };
      const notesCreated: NoteEntity[] = notesCrud.createNotes([noteInput]);
      expect(notesCreated).toHaveLength(1);
      expect(notesCreated[0]).toMatchObject(noteInput);
    });

    test("Create two notes test", () => {
      const noteInput: inputCreateNote[] = [{
        title: faker.word.adjective(),
        content: faker.word.words(),
      }, {
        title: faker.word.adjective(),
        content: faker.word.words(),
      }];
      const notesCreated: NoteEntity[] = notesCrud.createNotes(noteInput);
      expect(notesCreated).toHaveLength(notesCreated.length);
      noteInput.forEach((noteInput: inputCreateNote, index: number) => {
        expect(notesCreated[index]).toMatchObject(noteInput);
      });
    });
  });

  describe("Get notes test", () => {
    beforeEach(() => clearAllNotes());
    test("Get one note test", () => {
      const noteInput: inputCreateNote = {
        title: faker.word.adjective(),
        content: faker.word.words(),
      };
      const notesCreated: NoteEntity[] = notesCrud.createNotes([noteInput]);

      const noteFound: NoteEntity = notesCrud.getNoteById(notesCreated[0].id) as NoteEntity;
      expect(noteFound).toMatchObject(notesCreated[0]);
    });

    test("Get all notes (4) test", () => {
      const notesForCreate = 4;
      createNotes(notesForCreate);
      const notesFound: NoteEntity[] = notesCrud.getAllNotes();
      expect(notesFound).toHaveLength(notesForCreate);
    });
  });

  describe("Update notes test", () => {
    beforeEach(() => clearAllNotes());
    test("Update two notes test", () => {
      const notesForCreate = 2;
      const notesCreated: NoteEntity[] = createNotes(notesForCreate) as NoteEntity[];
      const inputUpdateNotes: inputUpdateNote[] = notesCreated.map(() => ({
        title: faker.word.adjective(),
        content: faker.word.words(),
      }))
      const { notesUpdated }: IReponseUpdateNotes = notesCrud.updateNotes(notesCreated.map(noteI => noteI.id), inputUpdateNotes);
      expect(notesUpdated).toHaveLength(notesForCreate);
      inputUpdateNotes.forEach((noteInputU: inputUpdateNote, index: number) => {
        expect(notesUpdated[index]).toMatchObject(noteInputU);
      });
    });
  });

  describe("Delete notes test", () => {
    beforeEach(() => clearAllNotes());
    test("Delete two notes test", () => {
      const notesForCreate = 3;
      const notesCreated: NoteEntity[] = createNotes(notesForCreate) as NoteEntity[];

      const noteIdsForDelete: string[] = notesCreated.map(noteI => noteI.id);
      const noteIdNotDeleted: string = noteIdsForDelete.pop() as string;
      const noteNotDeleted: NoteEntity = notesCreated.find(noteI => noteI.id === noteIdNotDeleted) as NoteEntity;

      const { notesIdsNotDeleted }: IReponseDeleteNotes = notesCrud.deleteNotes(noteIdsForDelete);
      expect(notesIdsNotDeleted).toHaveLength(0);

      const noteFound: NoteEntity = notesCrud.getNoteById(noteIdNotDeleted) as NoteEntity;
      expect(noteFound).toMatchObject(noteNotDeleted);
    });
  });

});