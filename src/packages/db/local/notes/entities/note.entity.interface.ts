interface NoteEntity {
  id: string;
  title: string;
  content: string;
}

export {
  NoteEntity
};