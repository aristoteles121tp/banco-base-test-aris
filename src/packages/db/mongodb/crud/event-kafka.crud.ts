import { IModelEventKafka } from "../interfaces";
import { ModelEventKafka } from "../models";

class EventKAfkaCRUD {
  async createOne(input: IModelEventKafka): Promise<IModelEventKafka> {
    const eventKafka: IModelEventKafka = new ModelEventKafka(input);
    await eventKafka.save();
    return eventKafka;
  }
}

export {
  EventKAfkaCRUD
}