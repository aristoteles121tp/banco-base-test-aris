import { Document } from "mongoose"

export interface IModelEventKafka extends Document {
  value: string; // buffer
  size: number;
  key: string; // buffer
  topic: string;
  offset: number;
  partition: number;
  timestamp: number;
}