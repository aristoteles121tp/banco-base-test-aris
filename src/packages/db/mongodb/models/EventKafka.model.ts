import { Model, model } from "mongoose";
import { IModelEventKafka } from "../interfaces";
import { schemaEventKafka } from "../schemas";

const ModelEventKafka: Model<IModelEventKafka> = model<IModelEventKafka>(
  'EventKafka',
  schemaEventKafka
);

export {
  ModelEventKafka
};
