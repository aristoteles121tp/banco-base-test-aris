import mongoose from 'mongoose';
// import { ModelEventKafka } from './models';

class MongooseService {

  public async initConnection() {
    try {
      const mongoURI: string = `${process.env.MONGODB_URL}:${process.env.MONGODB_PORT}`;
      console.log(mongoURI);
      const connection: mongoose.Mongoose = await mongoose.connect(mongoURI, {
        connectTimeoutMS: 15000,
        socketTimeoutMS: 10000,
        user: process.env.MONGODB_USER,
        pass: process.env.MONGODB_PWD,
        dbName: process.env.MONGODB_DB_NAME,
      });
      console.log('Mongodb connection successful');
      // await ModelEventKafka.createCollection();
      return connection;
    } catch(error) {
      console.error('Error to connect with mongodb', error);
      throw error;
    }
  }

  public setAutoReconnect() {
    mongoose.connection.on('disconnected', () => {
      this.initConnection();
    });
    mongoose.connection.on('error', (error) => {
      console.log(
        'Mongoose default connection has occured ' + error + ' error'
      );
      this.initConnection();
    });
  }

  public setEvents(url: string) {
    mongoose.connection.on('connected', () => {
      console.log(`Mongoose default connection is open to ${url}`);
    });

    mongoose.connection.on('error', (error) => {
      console.log(
        'Mongoose default connection has occured ' + error + ' error'
      );
    });

    mongoose.connection.on('disconnected', () => {
      console.log('Mongoose default connection is disconnected');
    });

    process.on('SIGINT', () => {
      mongoose.connection.close(true);
    });
  }

  public async disconnect() {
    await mongoose.connection.close();
  }
}

export {
  MongooseService
};

