import { Schema } from "mongoose";

class SchemaEventKafka extends Schema {
  constructor() {
    super(
      {
        value: {
          type: String,
          required: true,
        },
        size: {
          type: Number,
          required: true,
        },
        key: {
          type: String,
          required: true,
        },
        topic: {
          type: String,
          required: true,
        },
        offset: {
          type: Number,
          required: true,
        },
        partition: {
          type: Number,
          required: true,
        },
        timestamp: {
          type: Number,
          required: true,
        },
      },
      {
        timestamps: {
          createdAt: true,
          updatedAt: true,
        },
      }
    );
  }
}

const schemaEventKafka = new SchemaEventKafka();

export {
  schemaEventKafka
};