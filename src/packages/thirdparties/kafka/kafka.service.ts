/* eslint-disable @typescript-eslint/no-explicit-any */
import Kafka from 'node-rdkafka';
import { EventKAfkaCRUD } from '../../db/mongodb';

class KafkaService {

  private readonly config: { [keyof: string]: string | undefined };
  private topicName: string | undefined;
  private producer: Kafka.Producer | undefined;
  private consumer: Kafka.KafkaConsumer | undefined;

  constructor() {
    this.config = {};
    this.loadConfig();
  }

  private loadConfig(): void {
    this.topicName = process.env.KAFKA_TOPICNAME;
    this.config['bootstrap.servers'] = process.env.KAFKA_SERVER;
    this.config['security.protocol'] = 'SASL_SSL';
    this.config['sasl.mechanisms'] = 'PLAIN';
    this.config['sasl.username'] = process.env.KAFKA_USERNAME;
    this.config['sasl.password'] = process.env.KAFKA_PWD;
    this.config["group.id"] = "node-group";
  }

  private async createProducer(): Promise<boolean> {
    const producer = new Kafka.Producer(this.config);
    producer.connect();
    return new Promise((resolve, reject) => {
      producer
        .on('ready', () => {
          console.log('Kafka Producer ready...');
          this.producer = producer;
          return resolve(true);
        })
        .on('delivery-report', (err, report) => {
          if (err) {
            console.warn('Error producing', err);
          } else {
            const { topic, key, value } = report;
            const k = (key || '').toString().padEnd(10, ' ');
            console.log(`Produced event to topic ${topic}: key = ${k} value = ${value}`);
          }
        })
        .on('event.error', (err) => {
          console.error('event.error to create producer', err);
          return reject(err);
        });
    });

  }

  private async createConsumer(): Promise<boolean> {
    const consumer = new Kafka.KafkaConsumer(this.config, { "auto.offset.reset": "earliest" });
    consumer.connect();

    return new Promise((resolve, reject) => {
      consumer
        .on('ready', () => {
          consumer.subscribe([this.topicName as string]);
          consumer.consume();
          console.log('Kafka consumer ready...');
          this.consumer = consumer;
          return resolve(true);
        })
        .on("data", async (message: Kafka.Message) => {
          console.log("Consumed message", message);
          console.log(JSON.stringify({
            ...message,
            valueString: message.value?.toString(),
            keyString: message.key?.toString()
          }, null, 2));
          const eventcafkaCrud = new EventKAfkaCRUD();
          await eventcafkaCrud.createOne(message as any);
        }).on('event.error', (err) => {
          console.error('event.error to create consumer', err);
          return reject(err);
        });
    });

  }

  async startServices(): Promise<void> {
    await this.createProducer();
    await this.createConsumer();
    return;
  }

  sendMessage(message: any, key: string): void {
    const value: string = typeof message === 'object' ? JSON.stringify(message, null, 2) : message;
    this.producer?.produce(this.topicName as string, -1, Buffer.from(value), Buffer.from(key));
  }

}

export {
  KafkaService,
};