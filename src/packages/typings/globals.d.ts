import { KafkaService } from "../thirdparties/kafka/kafka.service";

declare global {
  const kafkaService: KafkaService;
  declare namespace Express {
    export interface Request {
      id: string;
    }
  }
}
